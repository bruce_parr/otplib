﻿using System;
using Ninject;
using OTPLib;

namespace OTPLibTestHarness
{
	class User
	{
		public string UserName {
			get { return "bparr"; }
		}

		public string Password {
			get { return "password"; }
		}

		public string YubiKeyUserTag {
			get { return "ccccccfivcnr"; }
		}
	}

	class MainClass
	{
		public static void Main (string[] args)
		{
			string yubicoClientId = "28972";
			string yubicoApiKey = "F7z8a3DG4BLq9W/L/IKU2SzfpiU=";

			User user = new User ();

			IKernel kernel = new StandardKernel (new NinjectBindingModule (yubicoApiKey, yubicoClientId));

			try {
				var processor = kernel.Get<IOTP> ("YubicoOTP");

				processor.OTPResponseHandler += new OTPResponseHandler (OnOTPResponse);

				while (true) {
					try {
						Console.WriteLine ("Please enter your username");
						Console.Write ("Username: ");
						string inputUserName = Console.ReadLine ();

						Console.WriteLine ("Please enter your password");
						Console.Write ("Password: ");
						string inputPassword = Console.ReadLine ();

						if (inputUserName == user.UserName && inputPassword == user.Password) {
							Console.WriteLine ("Press your Yubikey now");
							Console.Write ("Yubikey OTP: ");
							string otp = Console.ReadLine ();

							//Check to see if this Yubikey belongs to the user trying to log in...
							if (otp.StartsWith (user.YubiKeyUserTag)) {
								//Since it belongs to the current user, lets process the one-time password...
								processor.ProcessOTP (otp);		
							} else {
								Console.WriteLine ("The Yubikey you used does not belong to this user!");
							}

						} else {
							Console.WriteLine ("Username or password were incorrect");
						}

						Console.WriteLine ("Try another? (Y/N)");
						var key = Console.ReadKey ();
						Console.ReadLine ();
						if (key.KeyChar == 'N' || key.KeyChar == 'n') {
							break;
						}

					} catch (Exception ex) {
						Console.WriteLine (ex.Message);
					}
				}
			} catch (Exception ex) {
				Console.WriteLine (ex.Message);
			}
			Environment.Exit (1);

		}

		static void OnOTPResponse (object sender, OTPEvent e)
		{
			
			if (e.IsSuccess) {
				Console.WriteLine ("Let's log in...");
			} else {
				Console.WriteLine (string.Format ("Error: {0}", e.ErrorMessage));
			}
		}
	}
}
