﻿using System;
using Ninject.Modules;
using OTPLib;

namespace OTPLibTestHarness
{
	public class NinjectBindingModule : NinjectModule
	{
		private readonly string _yubicoClientId;
		private readonly string _yubicoApiKey;

		public NinjectBindingModule (string yubicoApiKey, string yubicoClientId)
		{
			_yubicoApiKey = yubicoApiKey;
			_yubicoClientId = yubicoClientId;
		}

		#region implemented abstract members of NinjectModule

		public override void Load ()
		{
//			string yubicoClientId = "28972";
//			string yubicoApiKey = "F7z8a3DG4BLq9W/L/IKU2SzfpiU=";

			Bind<IOTP> ().To<YubicoOTP> ().Named ("YubicoOTP").WithConstructorArgument ("clientId", _yubicoClientId).WithConstructorArgument ("apiKey", _yubicoApiKey);
			Bind<IOTP> ().To<GoogleOTP> ().Named ("GoogleOTP");
		}

		#endregion
	}
}

