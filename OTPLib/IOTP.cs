﻿using System;

namespace OTPLib
{
	public interface IOTP
	{
		event OTPResponseHandler OTPResponseHandler;

		void ProcessOTP (string OTPString);

		void FireEvent (bool isSuccess, string ErrorMessage);

	}
}

