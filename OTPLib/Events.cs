﻿using System;

namespace OTPLib
{
	public delegate void OTPResponseHandler (object sender, OTPEvent e);
	public class OTPEvent : EventArgs
	{
		public bool IsSuccess;
		public string ErrorMessage;
	}
}

