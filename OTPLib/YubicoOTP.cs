﻿using System;

namespace OTPLib
{
	public class YubicoOTP : IOTP
	{
		private readonly string _clientId;
		private readonly string _apiKey;

		public YubicoOTP (string clientId, string apiKey)
		{
			_clientId = clientId;
			_apiKey = apiKey;
		}

		#region IOTP implementation

		public event OTPResponseHandler OTPResponseHandler;

		public void ProcessOTP (string otpString)
		{
			try {

				//Set up the client and pass in the client ID from the YubiCloud API
				YubicoDotNetClient.YubicoClient client = new YubicoDotNetClient.YubicoClient (_clientId);

				//Set the API key with the key we also got from the YubiCloud API
				client.SetApiKey (_apiKey);
				Console.WriteLine ("Sending Yubico verify request...");

				//Send the request to YubiCloud and wait for a response
				YubicoDotNetClient.IYubicoResponse response = client.Verify (otpString);

				//Got a response, so let's fire off our event.
				if (response.Status == YubicoDotNetClient.YubicoResponseStatus.Ok) {
					FireEvent (true, "");
				} else {
					FireEvent (false, string.Format ("There was an error attempting to authenticate: {0}", response.Status.ToString ()));
				}

			} catch (Exception ex) {
				throw;
			}
		}

		public void FireEvent (bool isSuccess, string message)
		{
			OTPEvent evt = new OTPEvent (); 
			evt.IsSuccess = isSuccess;
			evt.ErrorMessage = message;

			if (OTPResponseHandler != null) { 
				OTPResponseHandler (this, evt); 
			}
			evt = null; 	
		}

		#endregion
	}
}
