﻿using System;
using Google.Authenticator;

namespace OTPLib
{
	public class GoogleOTP : IOTP
	{
		private readonly string _clientId;
		private readonly string _apiKey;

		public GoogleOTP (string clientId, string apiKey)
		{
			_clientId = clientId;
			_apiKey = apiKey;
		}

		#region IOTP implementation

		public event OTPResponseHandler OTPResponseHandler;

		public void ProcessOTP (string otpString)
		{
			
		}

		public void FireEvent (bool isSuccess, string message)
		{
			
		}

		#endregion
	}
}

